from requests.exceptions import HTTPError

from .context import notifier

EMAIL_LIST = {
    '/alic_bioimages/': {'james.cleverly@uts.edu.au'},
    '/clpm_bioimages/': {'Wayne.meyer@adelaide.edu.au', 'georgia.koerber@adelaide.edu.au'},
    '/cblp_bioimages/': {'e.pendall@westernsydney.edu.au'},
    '/boya_bioimages/': {'jason.beringer@uwa.edu.au', 'M.Boer@westernsydney.edu.au'},
    '/ggbw_bioimages/': {'r.silberstein@ecu.edu.au'},
    '/seqp_karawatha_bioimages/': {}
}

def test_call_notify_slackchannel(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = True
    #assert notifier.notify_slack('/data/users/alic_bioimages', 'this is the message') ==  {'err': True, 'message': 'this is the subject. No configuration setup, so no notifications send for path /data/users/alic/'}
    assert notifier.notify_slack(
        '/data/users/alic_bioimages', 'this is the message') == {'err': ''}
    notifier.inform_slackchannel.assert_called_with('this is the message')


def test_send_emails(mocker):
    import smtplib
    from unittest.mock import MagicMock

    mocker.patch.dict(
        'os.environ', {'SMTP_PASSWORD': 'testing', 'SMTP_USER': 'testing'})

    smtp_mock = MagicMock()
    mocker.patch.object(smtplib, 'SMTP', MagicMock(return_value=smtp_mock))

    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = True

    assert notifier.send_emails('/alic_bioimages/', ['test.user@testing.com'],
                                'this is the subject', 'this is the message') == {'err': True, 'ok': True}
    notifier.inform_slackchannel.assert_called_with(
        'this is the subject for path /alic_bioimages/ notification was send to test.user@testing.com')


def test_notify_email_unconfigured_path_no_emails_send(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = True

    assert notifier.notify_email('/data/users/alic', 'this is the message', 'this is the subject') == {
        'err': True, 'message': "this is the subject. No configuration setup, so no notifications send for path /data/users/alic"}
    notifier.inform_slackchannel.assert_called_with(
        "this is the subject. No configuration setup, so no notifications send for path /data/users/alic")

def test_notify_email_no_recipients(mocker):
    #client = MagicMock()
    #client.list.return_value = {
    #    '/alic_bioimages/', 'incomplete.log', 'image.jpg'}
    #values = {'/alic_bioimages/notified.log': False,
    #          '/alic_bioimages/error.log': False,
    #          '/alic_bioimages/incomplete.log': {'/alic_bioimages/incomplete.log'}}
    #def side_effect(filename):
    #    return values[filename]
    #client.check.side_effect = side_effect
    mocker.patch.object(notifier, 'email_by_path')
    notifier.email_by_path.return_value = EMAIL_LIST

    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = True

    assert notifier.notify_email('/data/users/seqp_karawatha_bioimages/', 'this is the message', 'this is the subject') == {
        'err': True, 'message': "this is the subject. No PI's configured, so no notifications send for path /data/users/seqp_karawatha_bioimages/"}
    notifier.inform_slackchannel.assert_called_with(
        "this is the subject. No PI's configured, so no notifications send for path /data/users/seqp_karawatha_bioimages/")


def test_notify_email_success(mocker):
    import smtplib
    from unittest.mock import MagicMock

    mocker.patch.object(notifier, 'email_by_path')
    notifier.email_by_path.return_value = EMAIL_LIST

    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = HTTPError

    mocker.patch.dict(
        'os.environ', {'SMTP_PASSWORD': 'testing', 'SMTP_USER': 'testing'})

    smtp_mock = MagicMock()
    mocker.patch.object(smtplib, 'SMTP', MagicMock(return_value=smtp_mock))

    assert notifier.notify_email('/data/users/alic_bioimages/',
                                 'this is the message', 'this is the subject') == {'err': False}
    notifier.inform_slackchannel.assert_called_with(
        'this is the subject for path /alic_bioimages/ notification was send to james.cleverly@uts.edu.au')


def test_notify_email_no_message(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = HTTPError
    mocker.patch.object(notifier, 'send_emails')
    notifier.send_emails.return_value = {'err': '', 'ok': True}
    assert notifier.notify_email(
        '/data/users/alic_bioimages/', None, 'this is the subject') == {'err': True, 'message': 'please provide a message'}


def test_notify_email_no_subject(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = HTTPError
    mocker.patch.object(notifier, 'send_emails')
    notifier.send_emails.return_value = {'err': '', 'ok': True}
    assert notifier.notify_email(
        '/data/users/alic_bioimages/', 'this is the message', None) == {'err': True, 'message': 'please provide a subject'}


def test_notify_email_no_path(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = HTTPError
    mocker.patch.object(notifier, 'send_emails')
    notifier.send_emails.return_value = {'err': '', 'ok': True}
    assert notifier.notify_email(
        None, 'this is the message', 'this is the subject') == {'err': True, 'message': 'please provide a path'}


def test_notify_email_exception(mocker):
    mocker.patch.object(notifier, 'inform_slackchannel')
    notifier.inform_slackchannel.return_value = False
    mocker.patch.object(notifier, 'send_emails')
    notifier.send_emails.return_value = {'err': '', 'ok': True}
    assert notifier.notify_email('', 'this is the message', 'this is the subject') == {
        'err': True, 'message': 'Error processing the notification, check slack channel - path '}
