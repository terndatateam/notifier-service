# Notification service

This package is created for the TERN Bioimages project, and sends a notification via email and/or Slack channel. For email, the recipients are based on the file path, notifications are send out to the configured email addresses for the path.

## Getting Started

### Installing

The following envirionment variables need to be setup

```
SMTP_PASSWORD=the_password
SMTP_USER=the_smtp_sender@email.com
BIO_SLACK_API=https://hooks.slack.com/services/your/own/api/location
BIO_PRD=1 # this parameter is set in the production environments to 1, for other environment it's optional or set to 0
```

Setup the virtual environment, and call pip install to install the required packages

```
python3 -m venv notification
. notification/bin/activate
pip install -r requirements.txt
```

## Running the tests

The tests are run using

```
pytest tests/test_mock_current.py
```

To view the code coverage

```
pytest --cov=notifier tests
```

## Authors

* **Wilma Karsdorp* - *Initial work* - [TERN](https://bitbucket.org/terndatateam)

## License

This project is licensed under the Content in this publication is licensed under Creative Commons
Attribution 4.0 International Licence, available at http://creativecommons.org/licenses/by/4.0/
