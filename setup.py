import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='Notifier',
    version='0.1.0',
    author='Wilma Karsdorp ',
    author_email='w.karsdorp@uq.edu.au',
    url='https://bitbucket.org/terndatateam/notifier-service',
    license='LICENSE',
    description='TERN notifier service to send emails and messages to the Slack channel',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        "requests == 2.22.0",
    ],
)