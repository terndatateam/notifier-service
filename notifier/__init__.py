#!/usr/bin/python
import os
import syslog
import json
import smtplib
import logging

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTPAuthenticationError

import requests
from requests.exceptions import HTTPError

logger = logging.getLogger('notifier')
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

def email_by_path():
    ADMIN_EMAIL = 'mirko.karan@jcu.edu.au'
    return {
        '/alic_bioimages/': {'james.cleverly@uts.edu.au', ADMIN_EMAIL},
        '/clpm_bioimages/': {'Wayne.meyer@adelaide.edu.au', 'georgia.koerber@adelaide.edu.au', ADMIN_EMAIL},
        '/cblp_bioimages/': {'e.pendall@westernsydney.edu.au', ADMIN_EMAIL},
        '/boya_bioimages/': {'jason.beringer@uwa.edu.au', 'M.Boer@westernsydney.edu.au', ADMIN_EMAIL},
        '/ggbw_bioimages/': {'r.silberstein@ecu.edu.au', ADMIN_EMAIL},
        '/gwwl_bioimages/': {'Craig.Macfarlane@csiro.au', 'Suzanne.Prober@csiro.au', 'Georg.Wiehl@csiro.au', ADMIN_EMAIL},
        '/lfld_bioimages/': {'Lindsay.Hutley@cdu.edu.au', 'Matthew.Northwood@cdu.edu.au', ADMIN_EMAIL},
        '/mgrl_bioimages/': {'pr.grace@qut.edu.au', 'd1.tucker@qut.edu.au', ADMIN_EMAIL},
        '/seqp_samford_bioimages/': {'pr.grace@qut.edu.au', 'd1.tucker@qut.edu.au', ADMIN_EMAIL},
        '/seqp_karawatha_bioimages/': {ADMIN_EMAIL},
        '/fnqr_robson_bioimages/': {'michael.liddell@jcu.edu.au', 'nico.weigand@jcu.edu.au', ADMIN_EMAIL},
        '/fnqr_daintree_bioimages/': {'michael.liddell@jcu.edu.au', 'nico.weigand@jcu.edu.au', ADMIN_EMAIL},
        '/ddc_bioimages/': {'michael.liddell@jcu.edu.au', 'Matt.Bradford@csiro.au', 'nico.weigand@jcu.edu.au', ADMIN_EMAIL},
        '/cowb_bioimages/': {'michael.liddell@jcu.edu.au', 'nico.weigand@jcu.edu.au', ADMIN_EMAIL},
        '/tumb_bioimages/': {'Jacqui.Stol@csiro.au', 'Mark.Kitchen@csiro.au', ADMIN_EMAIL},
        '/wrra_bioimages/': {'timothy.wardlaw@utas.edu.au', 'alison.phillips@sttas.com.au', ADMIN_EMAIL},
        '/wsbe_bioimages/': {'sarndt@unimelb.edu.au', 'n.hinkonajera@unimelb.edu.au', ADMIN_EMAIL},
        '/stlucia_bioimages/': {'w.karsdorp@uq.edu.au', 'a.cleland@uq.edu.au', ADMIN_EMAIL},
        '/vicd_whroo_bioimages/': {ADMIN_EMAIL},
}

def inform_slackchannel(message):
    """Send message to Slack Channel

    Args:
        message (str): the message to send.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    headers = {'Cache-Control': 'no-cache', 'Content-Type': 'application/json'}
    payload = json.dumps({'text': message})
    try:
        response = requests.post(
            os.environ["BIO_SLACK_API"], data=payload, headers=headers)
        response.raise_for_status()
    except HTTPError as http_err:
        raise http_err
    return True


def send_emails(path, recipients, subject, message):
    """Send emails

    Args:
        path (str): the file path for which to send emails.
        recipients (str): the recipients.
        subject (str): the subject line for the message.
        message (str): the message to send.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    return_value = {'err': '', 'ok': True}
    org_recipients = recipients

    if not "BIO_PRD" in os.environ or os.environ.get("BIO_PRD") == 0:
        recipients = ['w.karsdorp@uq.edu.au']
    if os.environ.get("SMTP_USER") is None or os.environ.get("SMTP_PASSWORD") is None:
        return_value['err'] = 'The system not configured to send emails, setup of smtp_user and smtp_password missing.'
        return_value['ok'] = False
        return return_value
    # set up the SMTP server
    s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    s.starttls()
    s.login(os.environ["SMTP_USER"], os.environ["SMTP_PASSWORD"])
    msg = MIMEMultipart()       # create a message
    # setup the parameters of the message
    msg['From'] = 'no-reply@tern.org.au'
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = subject
    msg['Reply-to'] = "w.karsdorp@uq.edu.au"

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    try:
        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg
        message = subject + ' for path ' + path + ' notification was send to ' + ", ".join(org_recipients)
        result = inform_slackchannel(message)
        return_value['err'] = result
    except SMTPAuthenticationError as smtp_err:
        del msg
        message = subject + ' for path ' + path + ' notification were not send due to invalid SMTP credentials. Recipient: ' + ", ".join(org_recipients)
        result = inform_slackchannel(message)
        raise smtp_err

    return return_value

def notify_slack(path, message):
    """Notify the Slack channel

    Args:
        path (str): the file path involved to send.
        message (str): the message to send.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    logger.info('notify_slack process started')
    return_value = {'err': ''}

    try:
        if message is None:
            return_value['err'] = 'no message was provided'

        result = inform_slackchannel(message)
        if not result:
            raise Exception(result)

    except HTTPError as http_err:
        logger.exception(
            'notify_slack process unable to inform slack channel: ')
    except Exception as error:
        logger.exception('notify_slack process ended on error: ')
        # send to slack channel
        message = "Error in notify_slack: path:" + \
            path + " message: " + message + ", check the logs"
        return_value['err'] = 'Error processing the notification, check slack channel - path ' + path
        inform_slackchannel(message)
    finally:
        logger.info('notify_slack process finalised')

    return return_value


def notify_email(path, message, subject):
    """Notify via eamil

    Args:
        path (str): the file path involved to send.
        message (str): the message to send.
        subject (str): the subject of the message to send.

    Returns:
        bool: The return value. True for success, False otherwise.

    """
    logger.info('notify_email process started')
    return_value = {'err': False}

    try:
        recipients = {}

        datapath = path
        if message is None:
            return_value['message'] = 'please provide a message'
            return_value['err'] = True
            return return_value
        if path is None:
            return_value['message'] = 'please provide a path'
            return_value['err'] = True
            return return_value
        if subject is None:
            return_value['message'] = 'please provide a subject'
            return_value['err'] = True
            return return_value

        # get the path (arg1) and check if one of the keys is part of the file name
        if not any(ele in path for ele in email_by_path()):
            # if the path is not found in the list, send a slack error
            message = subject + ". No configuration setup, so no notifications send for path " + path
            return_value['err'] = True
            return_value['message'] = message
            result = inform_slackchannel(message)
            if not result:
                raise Exception(result)
        else:
            for key in email_by_path():
                if path.find(key) != -1:
                    recipients = email_by_path()[key]
                    raw_path = path.split(key)
                    datapath = key + raw_path[1]
                    break

            if len(recipients) > 0:
                # send out the email
                are_emails_send = send_emails(
                    datapath, recipients, subject, message)
                if not are_emails_send['ok']:
                    raise Exception('No emails were send. ' + are_emails_send['err'])
            else:
                # send to slack channel that no emails can be found
                message = subject + ". No PI's configured, so no notifications send for path " + path
                result = inform_slackchannel(message)
                if not result:
                    raise Exception(result)
                return_value['message'] = message
                return_value['err'] = True

    except HTTPError as http_err:
        logger.error(
            'notify_email process unable to inform slack channel: ')
    except Exception as err:
        logger.error('notify_email process ended on error: '+ repr(err))
        # send to slack channel
        message = "Error in notify_email:" + repr(err) + " subject: " + subject + " path: " + path
        if str(err).find('informSlackChannel') < 0:
            return_value['message'] = 'Error processing the notification, check slack channel - path ' + path
            return_value['err'] = True
            inform_slackchannel(message)
        else:
            return_value['message'] = 'Error processing the notification, unable to log error to slack channel - path ' + path
            return_value['err'] = True
    finally:
        logger.info('notify_email process finalised')

    return return_value
