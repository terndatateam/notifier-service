requests == 2.22.0
mock==3.0.5
pytest==5.0.1
pytest-cov==2.8.1
pytest-mock==2.0.0
jsonify==0.5